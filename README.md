# zemporiom-zsh-theme

Zetoph's minimal Zsh theme.

## Installation

Assuming you use [Oh my Zsh](https://ohmyz.sh/)
```
curl https://gitlab.com/zetoph/zemporium-zsh-theme/-/raw/main/zemporium.zsh-theme -o ~/.oh-my-zsh/themes/zemporium.zsh-theme
```

